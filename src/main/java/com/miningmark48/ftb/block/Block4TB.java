package com.miningmark48.ftb.block;

import com.miningmark48.ftb.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

public class Block4TB extends Block {
    public Block4TB(Material material) {

        super(material);

    }

    public Block4TB() {
        super(Material.rock);
        this.setHardness(2.0F);
    }
}