package com.miningmark48.ftb.block;

import com.miningmark48.ftb.reference.Reference;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class BlockFragileCrate extends Block4TB{

    public IIcon[] icons = new IIcon[6];

    public BlockFragileCrate(){
        super();
    }

    @Override
    public void registerBlockIcons(IIconRegister reg){
        for (int i = 0; i < 6; i++){
            if (i == 1 || i == 0){
                this.icons[i] = reg.registerIcon(this.textureName + "_top");
            }else if(i == 2 || i == 3 || i == 4 || i == 5){
                this.icons[i] = reg.registerIcon(this.textureName + "_side");
            }
        }
    }

}
