package com.miningmark48.ftb.creativetab;


import com.miningmark48.ftb.init.ModItems;
import com.miningmark48.ftb.reference.Reference;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTab4TB {

    public static final CreativeTabs FTB_TAB = new CreativeTabs(Reference.MOD_ID){

        @Override
        public Item getTabIconItem(){

            return ModItems.wand;

        }

    };


}

