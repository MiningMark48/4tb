package com.miningmark48.ftb.init;


import com.miningmark48.ftb.block.*;
import com.miningmark48.ftb.creativetab.CreativeTab4TB;
import com.miningmark48.ftb.reference.Reference;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;


@GameRegistry.ObjectHolder(Reference.MOD_ID)
public class ModBlocks {

    public static final Block emeraldOrange = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldOrange").setBlockName("emeraldOrange").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldBlack = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldBlack").setBlockName("emeraldBlack").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldPink = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldPink").setBlockName("emeraldPink").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldBlue = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldBlue").setBlockName("emeraldBlue").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldPurple = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldPurple").setBlockName("emeraldPurple").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldBrown = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldBrown").setBlockName("emeraldBrown").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldRed = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldRed").setBlockName("emeraldRed").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldCyan = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldCyan").setBlockName("emeraldCyan").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldSilver = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldSilver").setBlockName("emeraldSilver").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldGreen = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldGreen").setBlockName("emeraldGreen").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldWhite = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldWhite").setBlockName("emeraldWhite").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldGrey = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldGrey").setBlockName("emeraldGrey").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldYellow = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldYellow").setBlockName("emeraldYellow").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldLBlue = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldLBlue").setBlockName("emeraldLBlue").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldLime = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldLime").setBlockName("emeraldLime").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldMagenta = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldMagenta").setBlockName("emeraldMagenta").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block emeraldIce = new BlockEmerald().setBlockTextureName(Reference.MOD_ID + ":emeraldIce").setBlockName("emeraldIce").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block bory = new BlockBory().setBlockTextureName(Reference.MOD_ID + ":bory").setBlockName("bory");
    public static final Block xanadu = new BlockXanadu().setBlockTextureName(Reference.MOD_ID + ":xanadu").setBlockName("xanadu");
    public static final BlockTable table = new BlockTable(Material.rock);
    public static final Block crate = new BlockCrate().setBlockName("crate").setCreativeTab(CreativeTab4TB.FTB_TAB).setBlockTextureName(Reference.MOD_ID + ":crate");
    public static final Block fragileCrate = new BlockFragileCrate().setBlockName("fragileCrate").setBlockTextureName(Reference.MOD_ID + ":fragileCrate").setCreativeTab(CreativeTab4TB.FTB_TAB);

    //Paint Blocks
    public static final Block paintBlock1 = new BlockPaintBlock().setBlockName("paintBlock1").setBlockTextureName(Reference.MOD_ID + ":paintBlock1").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock2 = new BlockPaintBlock().setBlockName("paintBlock2").setBlockTextureName(Reference.MOD_ID + ":paintBlock2").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock3 = new BlockPaintBlock().setBlockName("paintBlock3").setBlockTextureName(Reference.MOD_ID + ":paintBlock3").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock4 = new BlockPaintBlock().setBlockName("paintBlock4").setBlockTextureName(Reference.MOD_ID + ":paintBlock4").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock5 = new BlockPaintBlock().setBlockName("paintBlock5").setBlockTextureName(Reference.MOD_ID + ":paintBlock5").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock6 = new BlockPaintBlock().setBlockName("paintBlock6").setBlockTextureName(Reference.MOD_ID + ":paintBlock6").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock7 = new BlockPaintBlock().setBlockName("paintBlock7").setBlockTextureName(Reference.MOD_ID + ":paintBlock7").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock8 = new BlockPaintBlock().setBlockName("paintBlock8").setBlockTextureName(Reference.MOD_ID + ":paintBlock8").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock9 = new BlockPaintBlock().setBlockName("paintBlock9").setBlockTextureName(Reference.MOD_ID + ":paintBlock9").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock10 = new BlockPaintBlock().setBlockName("paintBlock10").setBlockTextureName(Reference.MOD_ID + ":paintBlock10").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock11 = new BlockPaintBlock().setBlockName("paintBlock11").setBlockTextureName(Reference.MOD_ID + ":paintBlock11").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock12 = new BlockPaintBlock().setBlockName("paintBlock12").setBlockTextureName(Reference.MOD_ID + ":paintBlock12").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock13 = new BlockPaintBlock().setBlockName("paintBlock13").setBlockTextureName(Reference.MOD_ID + ":paintBlock13").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock14 = new BlockPaintBlock().setBlockName("paintBlock14").setBlockTextureName(Reference.MOD_ID + ":paintBlock14").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock15 = new BlockPaintBlock().setBlockName("paintBlock15").setBlockTextureName(Reference.MOD_ID + ":paintBlock15").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock16 = new BlockPaintBlock().setBlockName("paintBlock16").setBlockTextureName(Reference.MOD_ID + ":paintBlock16").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock17 = new BlockPaintBlock().setBlockName("paintBlock17").setBlockTextureName(Reference.MOD_ID + ":paintBlock17").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock18 = new BlockPaintBlock().setBlockName("paintBlock18").setBlockTextureName(Reference.MOD_ID + ":paintBlock18").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock19 = new BlockPaintBlock().setBlockName("paintBlock19").setBlockTextureName(Reference.MOD_ID + ":paintBlock19").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock20 = new BlockPaintBlock().setBlockName("paintBlock20").setBlockTextureName(Reference.MOD_ID + ":paintBlock20").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock21 = new BlockPaintBlock().setBlockName("paintBlock21").setBlockTextureName(Reference.MOD_ID + ":paintBlock21").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock22 = new BlockPaintBlock().setBlockName("paintBlock22").setBlockTextureName(Reference.MOD_ID + ":paintBlock22").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock23 = new BlockPaintBlock().setBlockName("paintBlock23").setBlockTextureName(Reference.MOD_ID + ":paintBlock23").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock24 = new BlockPaintBlock().setBlockName("paintBlock24").setBlockTextureName(Reference.MOD_ID + ":paintBlock24").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock25 = new BlockPaintBlock().setBlockName("paintBlock25").setBlockTextureName(Reference.MOD_ID + ":paintBlock25").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock26 = new BlockPaintBlock().setBlockName("paintBlock26").setBlockTextureName(Reference.MOD_ID + ":paintBlock26").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock27 = new BlockPaintBlock().setBlockName("paintBlock27").setBlockTextureName(Reference.MOD_ID + ":paintBlock27").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock28 = new BlockPaintBlock().setBlockName("paintBlock28").setBlockTextureName(Reference.MOD_ID + ":paintBlock28").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock29 = new BlockPaintBlock().setBlockName("paintBlock29").setBlockTextureName(Reference.MOD_ID + ":paintBlock29").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock30 = new BlockPaintBlock().setBlockName("paintBlock30").setBlockTextureName(Reference.MOD_ID + ":paintBlock30").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock31 = new BlockPaintBlock().setBlockName("paintBlock31").setBlockTextureName(Reference.MOD_ID + ":paintBlock31").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock32 = new BlockPaintBlock().setBlockName("paintBlock32").setBlockTextureName(Reference.MOD_ID + ":paintBlock32").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block paintBlock33 = new BlockPaintBlock().setBlockName("paintBlock33").setBlockTextureName(Reference.MOD_ID + ":paintBlock33").setCreativeTab(CreativeTab4TB.FTB_TAB);

    //Groovy Blocks
    public static final Block groovyBlock1 = new BlockGroovy().setBlockName("groovyBlock1").setBlockTextureName(Reference.MOD_ID + ":groovyBlock1").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block groovyBlock2 = new BlockGroovy().setBlockName("groovyBlock2").setBlockTextureName(Reference.MOD_ID + ":groovyBlock2").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block groovyBlock3 = new BlockGroovy().setBlockName("groovyBlock3").setBlockTextureName(Reference.MOD_ID + ":groovyBlock3").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block groovyBlock4 = new BlockGroovy().setBlockName("groovyBlock4").setBlockTextureName(Reference.MOD_ID + ":groovyBlock4").setCreativeTab(CreativeTab4TB.FTB_TAB);

    //RIF Wool
    public static final Block rifWool_lightBlue = new BlockReinforced().setBlockName("rifWool_lightBlue").setBlockTextureName(Reference.MOD_ID + ":rifWool_lightBlue").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_lightGrey = new BlockReinforced().setBlockName("rifWool_lightGrey").setBlockTextureName(Reference.MOD_ID + ":rifWool_lightGrey").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_lime = new BlockReinforced().setBlockName("rifWool_lime").setBlockTextureName(Reference.MOD_ID + ":rifWool_lime").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_magenta = new BlockReinforced().setBlockName("rifWool_magenta").setBlockTextureName(Reference.MOD_ID + ":rifWool_magenta").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_orange = new BlockReinforced().setBlockName("rifWool_orange").setBlockTextureName(Reference.MOD_ID + ":rifWool_orange").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_pink = new BlockReinforced().setBlockName("rifWool_pink").setBlockTextureName(Reference.MOD_ID + ":rifWool_pink").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_purple = new BlockReinforced().setBlockName("rifWool_purple").setBlockTextureName(Reference.MOD_ID + ":rifWool_purple").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_red = new BlockReinforced().setBlockName("rifWool_red").setBlockTextureName(Reference.MOD_ID + ":rifWool_red").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_white = new BlockReinforced().setBlockName("rifWool_white").setBlockTextureName(Reference.MOD_ID + ":rifWool_white").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_yellow = new BlockReinforced().setBlockName("rifWool_yellow").setBlockTextureName(Reference.MOD_ID + ":rifWool_yellow").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_black = new BlockReinforced().setBlockName("rifWool_black").setBlockTextureName(Reference.MOD_ID + ":rifWool_black").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_blue = new BlockReinforced().setBlockName("rifWool_blue").setBlockTextureName(Reference.MOD_ID + ":rifWool_blue").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_brown = new BlockReinforced().setBlockName("rifWool_brown").setBlockTextureName(Reference.MOD_ID + ":rifWool_brown").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_cyan = new BlockReinforced().setBlockName("rifWool_cyan").setBlockTextureName(Reference.MOD_ID + ":rifWool_cyan").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_green = new BlockReinforced().setBlockName("rifWool_green").setBlockTextureName(Reference.MOD_ID + ":rifWool_green").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block rifWool_grey = new BlockReinforced().setBlockName("rifWool_grey").setBlockTextureName(Reference.MOD_ID + ":rifWool_grey").setCreativeTab(CreativeTab4TB.FTB_TAB);

    //Camo
    public static final Block camo1 = new BlockCamo().setBlockName("camo1").setBlockTextureName(Reference.MOD_ID + ":camo1").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo2 = new BlockCamo().setBlockName("camo2").setBlockTextureName(Reference.MOD_ID + ":camo2").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo3 = new BlockCamo().setBlockName("camo3").setBlockTextureName(Reference.MOD_ID + ":camo3").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo4 = new BlockCamo().setBlockName("camo4").setBlockTextureName(Reference.MOD_ID + ":camo4").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo5 = new BlockCamo().setBlockName("camo5").setBlockTextureName(Reference.MOD_ID + ":camo5").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo6 = new BlockCamo().setBlockName("camo6").setBlockTextureName(Reference.MOD_ID + ":camo6").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo7 = new BlockCamo().setBlockName("camo7").setBlockTextureName(Reference.MOD_ID + ":camo7").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo8 = new BlockCamo().setBlockName("camo8").setBlockTextureName(Reference.MOD_ID + ":camo8").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo9 = new BlockCamo().setBlockName("camo9").setBlockTextureName(Reference.MOD_ID + ":camo9").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo10 = new BlockCamo().setBlockName("camo10").setBlockTextureName(Reference.MOD_ID + ":camo10").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block camo11 = new BlockCamo().setBlockName("camo11").setBlockTextureName(Reference.MOD_ID + ":camo11").setCreativeTab(CreativeTab4TB.FTB_TAB);

    //Fences
    public static final Block fence1 = new BlockModPane(Reference.MOD_ID + ":fence1", Reference.MOD_ID + ":fence1", Material.iron, true).setBlockName("fence1").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block fence2 = new BlockModPane(Reference.MOD_ID + ":fence2", Reference.MOD_ID + ":fence2", Material.iron, true).setBlockName("fence2").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Block fence3 = new BlockModPane(Reference.MOD_ID + ":fence3", Reference.MOD_ID + ":fence3", Material.iron, true).setBlockName("fence3").setCreativeTab(CreativeTab4TB.FTB_TAB);



    public static void init(){
        GameRegistry.registerBlock(emeraldOrange, "emeraldOrange");
        GameRegistry.registerBlock(emeraldBlack, "emeraldBlack");
        GameRegistry.registerBlock(emeraldPink, "emeraldPink");
        GameRegistry.registerBlock(emeraldBlue, "emeraldBlue");
        GameRegistry.registerBlock(emeraldPurple, "emeraldPurple");
        GameRegistry.registerBlock(emeraldBrown, "emeraldBrown");
        GameRegistry.registerBlock(emeraldRed, "emeraldRed");
        GameRegistry.registerBlock(emeraldCyan, "emeraldCyan");
        GameRegistry.registerBlock(emeraldSilver, "emeraldSilver");
        GameRegistry.registerBlock(emeraldGreen, "emeraldGreen");
        GameRegistry.registerBlock(emeraldWhite, "emeraldWhite");
        GameRegistry.registerBlock(emeraldGrey, "emeraldGrey");
        GameRegistry.registerBlock(emeraldYellow, "emeraldYellow");
        GameRegistry.registerBlock(emeraldLBlue, "emeraldLBlue");
        GameRegistry.registerBlock(emeraldLime, "emeraldLime");
        GameRegistry.registerBlock(emeraldMagenta, "emeraldMagenta");
        GameRegistry.registerBlock(emeraldIce, "emeraldIce");

        GameRegistry.registerBlock(paintBlock1, "paintBlock1");
        GameRegistry.registerBlock(paintBlock2, "paintBlock2");
        GameRegistry.registerBlock(paintBlock3, "paintBlock3");
        GameRegistry.registerBlock(paintBlock4, "paintBlock4");
        GameRegistry.registerBlock(paintBlock5, "paintBlock5");
        GameRegistry.registerBlock(paintBlock6, "paintBlock6");
        GameRegistry.registerBlock(paintBlock7, "paintBlock7");
        GameRegistry.registerBlock(paintBlock8, "paintBlock8");
        GameRegistry.registerBlock(paintBlock9, "paintBlock9");
        GameRegistry.registerBlock(paintBlock10, "paintBlock10");
        GameRegistry.registerBlock(paintBlock11, "paintBlock11");
        GameRegistry.registerBlock(paintBlock12, "paintBlock12");
        GameRegistry.registerBlock(paintBlock13, "paintBlock13");
        GameRegistry.registerBlock(paintBlock14, "paintBlock14");
        GameRegistry.registerBlock(paintBlock15, "paintBlock15");
        GameRegistry.registerBlock(paintBlock16, "paintBlock16");
        GameRegistry.registerBlock(paintBlock17, "paintBlock17");
        GameRegistry.registerBlock(paintBlock18, "paintBlock18");
        GameRegistry.registerBlock(paintBlock19, "paintBlock19");
        GameRegistry.registerBlock(paintBlock20, "paintBlock20");
        GameRegistry.registerBlock(paintBlock21, "paintBlock21");
        GameRegistry.registerBlock(paintBlock22, "paintBlock22");
        GameRegistry.registerBlock(paintBlock23, "paintBlock23");
        GameRegistry.registerBlock(paintBlock24, "paintBlock24");
        GameRegistry.registerBlock(paintBlock25, "paintBlock25");
        GameRegistry.registerBlock(paintBlock26, "paintBlock26");
        GameRegistry.registerBlock(paintBlock27, "paintBlock27");
        GameRegistry.registerBlock(paintBlock28, "paintBlock28");
        GameRegistry.registerBlock(paintBlock29, "paintBlock29");
        GameRegistry.registerBlock(paintBlock30, "paintBlock30");
        GameRegistry.registerBlock(paintBlock31, "paintBlock31");
        GameRegistry.registerBlock(paintBlock32, "paintBlock32");
        GameRegistry.registerBlock(paintBlock33, "paintBlock33");

        GameRegistry.registerBlock(groovyBlock1, "groovyBlock1");
        GameRegistry.registerBlock(groovyBlock2, "groovyBlock2");
        GameRegistry.registerBlock(groovyBlock3, "groovyBlock3");
        GameRegistry.registerBlock(groovyBlock4, "groovyBlock4");

        GameRegistry.registerBlock(rifWool_lightBlue, "rifWool_lightBlue");
        GameRegistry.registerBlock(rifWool_lightGrey, "rigWool_lightGrey");
        GameRegistry.registerBlock(rifWool_lime, "rigWool_lime");
        GameRegistry.registerBlock(rifWool_magenta, "rifWool_magenta");
        GameRegistry.registerBlock(rifWool_orange, "rifWool_orange");
        GameRegistry.registerBlock(rifWool_pink, "rifWool_pink");
        GameRegistry.registerBlock(rifWool_purple, "rifWool_purple");
        GameRegistry.registerBlock(rifWool_red, "rifWool_red");
        GameRegistry.registerBlock(rifWool_white, "rifWool_white");
        GameRegistry.registerBlock(rifWool_yellow, "rifWool_yellow");
        GameRegistry.registerBlock(rifWool_black, "rifWool_black");
        GameRegistry.registerBlock(rifWool_blue, "rifWool_blue");
        GameRegistry.registerBlock(rifWool_brown, "rifWool_brown");
        GameRegistry.registerBlock(rifWool_cyan, "rifWool_cyan");
        GameRegistry.registerBlock(rifWool_green, "rifWool_green");
        GameRegistry.registerBlock(rifWool_grey, "rifWool_grey");

        GameRegistry.registerBlock(camo1, "camo1");
        GameRegistry.registerBlock(camo2, "camo2");
        GameRegistry.registerBlock(camo3, "camo3");
        GameRegistry.registerBlock(camo4, "camo4");
        GameRegistry.registerBlock(camo5, "camo5");
        GameRegistry.registerBlock(camo6, "camo6");
        GameRegistry.registerBlock(camo7, "camo7");
        GameRegistry.registerBlock(camo8, "camo8");
        GameRegistry.registerBlock(camo9, "camo9");
        GameRegistry.registerBlock(camo10, "camo10");
        GameRegistry.registerBlock(camo11, "camo11");

        GameRegistry.registerBlock(fence1, "fence1");
        GameRegistry.registerBlock(fence2, "fence2");
        GameRegistry.registerBlock(fence3, "fence3");

        GameRegistry.registerBlock(bory, "bory");
        GameRegistry.registerBlock(xanadu, "xanadu");
        GameRegistry.registerBlock(table, "table");
        GameRegistry.registerBlock(crate, "crate");
        //GameRegistry.registerBlock(fragileCrate, "fragileCrate");

    }

}
