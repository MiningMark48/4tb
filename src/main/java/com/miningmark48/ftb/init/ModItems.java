package com.miningmark48.ftb.init;


import com.miningmark48.ftb.creativetab.CreativeTab4TB;
import com.miningmark48.ftb.item.*;
import com.miningmark48.ftb.reference.Reference;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;


@GameRegistry.ObjectHolder(Reference.MOD_ID)
public class ModItems {

    public static final Item ruby = new ItemRuby().setUnlocalizedName("ruby").setTextureName(Reference.MOD_ID + ":ruby").setCreativeTab(CreativeTab4TB.FTB_TAB);

    public static final Item iceIngot = new Item4TB().setUnlocalizedName("iceIngot").setTextureName(Reference.MOD_ID + ":iceIngot").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Item wandCap = new Item4TB().setUnlocalizedName("wandCap").setTextureName(Reference.MOD_ID + ":wandCap").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Item wandCore = new Item4TB().setUnlocalizedName("wandCore").setTextureName(Reference.MOD_ID + ":wandCore").setCreativeTab(CreativeTab4TB.FTB_TAB);
    public static final Item wand = new ItemWand().setUnlocalizedName("wand").setTextureName(Reference.MOD_ID + ":wand").setCreativeTab(CreativeTab4TB.FTB_TAB);

    public static void init(){

        GameRegistry.registerItem(ruby, "ruby");

        GameRegistry.registerItem(iceIngot, "iceIngot");
        GameRegistry.registerItem(wandCap, "wandCap");
        GameRegistry.registerItem(wandCore, "wandCore");
        GameRegistry.registerItem(wand, "wand");

    }

}
