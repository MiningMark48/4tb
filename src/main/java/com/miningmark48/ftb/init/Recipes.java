package com.miningmark48.ftb.init;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class Recipes {

    public static void init(){

        //Paint Blocks
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock1), new ItemStack(Blocks.stone), new ItemStack(Items.dye));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock1), new ItemStack(ModBlocks.paintBlock33));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock2), new ItemStack(ModBlocks.paintBlock1));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock3), new ItemStack(ModBlocks.paintBlock2));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock4), new ItemStack(ModBlocks.paintBlock3));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock5), new ItemStack(ModBlocks.paintBlock4));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock6), new ItemStack(ModBlocks.paintBlock5));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock7), new ItemStack(ModBlocks.paintBlock6));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock8), new ItemStack(ModBlocks.paintBlock7));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock9), new ItemStack(ModBlocks.paintBlock8));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock10), new ItemStack(ModBlocks.paintBlock9));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock11), new ItemStack(ModBlocks.paintBlock10));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock12), new ItemStack(ModBlocks.paintBlock11));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock13), new ItemStack(ModBlocks.paintBlock12));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock14), new ItemStack(ModBlocks.paintBlock13));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock15), new ItemStack(ModBlocks.paintBlock14));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock16), new ItemStack(ModBlocks.paintBlock15));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock17), new ItemStack(ModBlocks.paintBlock16));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock18), new ItemStack(ModBlocks.paintBlock17));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock19), new ItemStack(ModBlocks.paintBlock18));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock20), new ItemStack(ModBlocks.paintBlock19));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock21), new ItemStack(ModBlocks.paintBlock20));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock22), new ItemStack(ModBlocks.paintBlock21));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock23), new ItemStack(ModBlocks.paintBlock22));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock24), new ItemStack(ModBlocks.paintBlock23));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock25), new ItemStack(ModBlocks.paintBlock24));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock26), new ItemStack(ModBlocks.paintBlock25));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock27), new ItemStack(ModBlocks.paintBlock26));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock28), new ItemStack(ModBlocks.paintBlock27));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock29), new ItemStack(ModBlocks.paintBlock28));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock30), new ItemStack(ModBlocks.paintBlock29));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock31), new ItemStack(ModBlocks.paintBlock30));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock32), new ItemStack(ModBlocks.paintBlock31));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.paintBlock33), new ItemStack(ModBlocks.paintBlock32));

        //Groovy Blocks
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.groovyBlock1), new ItemStack(Blocks.wool), new ItemStack(Items.dye));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.groovyBlock1), new ItemStack(ModBlocks.groovyBlock4));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.groovyBlock2), new ItemStack(ModBlocks.groovyBlock1));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.groovyBlock3), new ItemStack(ModBlocks.groovyBlock2));
        GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.groovyBlock4), new ItemStack(ModBlocks.groovyBlock3));

    }

}
