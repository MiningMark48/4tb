package com.miningmark48.ftb.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemWand extends Item4TB{
	
	public ItemWand() {
		super();
		this.setUnlocalizedName("wand");
		this.setMaxStackSize(1);
	}

}
