
package com.miningmark48.ftb.model;


import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTable extends ModelBase
{
  //fields
    ModelRenderer Top;
    ModelRenderer Pole;
    ModelRenderer Base;
  
  public ModelTable()
  {
    textureWidth = 64;
    textureHeight = 64;
    
      Top = new ModelRenderer(this, 0, 0);
      Top.addBox(0F, 0F, 0F, 16, 1, 16);
      Top.setRotationPoint(-8F, 8F, -8F);
      Top.setTextureSize(64, 64);
      Top.mirror = true;
      setRotation(Top, 0F, 0F, 0F);
      Pole = new ModelRenderer(this, 0, 17);
      Pole.addBox(0F, 0F, 0F, 2, 14, 2);
      Pole.setRotationPoint(-1F, 9F, -1F);
      Pole.setTextureSize(64, 64);
      Pole.mirror = true;
      setRotation(Pole, 0F, 0F, 0F);
      Base = new ModelRenderer(this, 0, 36);
      Base.addBox(0F, 0F, 0F, 4, 1, 4);
      Base.setRotationPoint(-2F, 23F, -2F);
      Base.setTextureSize(64, 64);
      Base.mirror = true;
      setRotation(Base, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Top.render(f5);
    Pole.render(f5);
    Base.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
