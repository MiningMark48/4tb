package com.miningmark48.ftb.proxy;

import com.miningmark48.ftb.reference.RenderIds;
import com.miningmark48.ftb.renderer.RenderTable;
import com.miningmark48.ftb.tileentity.TileEntityTable;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy{
    @Override
    public ClientProxy getClientProxy() {
        return null;
    }

    @Override
    public void registerRenderThings(){
        //Table
        RenderIds.Table = RenderingRegistry.getNextAvailableRenderId();
        //MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.Table), new Item);
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTable.class, new RenderTable());

    }
    public void registerTileEntitySpecialRenderer(){

    }
}