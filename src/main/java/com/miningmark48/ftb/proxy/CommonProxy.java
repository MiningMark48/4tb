package com.miningmark48.ftb.proxy;

import com.miningmark48.ftb.reference.Names;
import com.miningmark48.ftb.tileentity.TileEntityTable;
import cpw.mods.fml.common.registry.GameRegistry;

public abstract class CommonProxy implements IProxy{
    public void registerTileEntities(){
        GameRegistry.registerTileEntityWithAlternatives(TileEntityTable.class, Names.Blocks.TABLE, "tile." + Names.Blocks.TABLE);
    }
}
