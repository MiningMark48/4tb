package com.miningmark48.ftb.proxy;

public interface IProxy {
    public abstract ClientProxy getClientProxy();
    public abstract void registerTileEntities();
    public abstract void registerRenderThings();

}
