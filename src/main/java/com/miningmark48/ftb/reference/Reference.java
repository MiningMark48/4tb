package com.miningmark48.ftb.reference;

public class Reference {

    public static final String MOD_ID = "ftb";
    public static final String MOD_NAME = "4 The Builders";
    public static final String VERSION = "1.7.10-0.0.1";
    public static final String CLIENT_PROXY_CLASS = "com.miningmark48.ftb.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.miningmark48.ftb.proxy.ServerProxy";
    public static final String GUI_FACTORY_CLASS = "com.miningmark48.ftb.gui.GuiFactory";

}
